import { Component, OnInit, Input,HostBinding } from '@angular/core';
import { persona } from '../models/persona';

@Component({
  selector: 'app-ver-personal',
  templateUrl: './ver-personal.component.html',
  styleUrls: ['./ver-personal.component.css']
})
export class VerPersonalComponent implements OnInit {
  @Input() person:persona;
  @HostBinding('attr.class') cssClass ="col-md-4";

  constructor() { }

  ngOnInit(): void {
  }

}
