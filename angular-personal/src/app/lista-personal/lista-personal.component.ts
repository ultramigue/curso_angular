import { Component, OnInit } from '@angular/core';
import { persona } from '../models/persona';

@Component({
  selector: 'app-lista-personal',
  templateUrl: './lista-personal.component.html',
  styleUrls: ['./lista-personal.component.css']
})
export class ListaPersonalComponent implements OnInit {
  personas:persona[];
  borrar:boolean;

  constructor() { 
    this.personas=[];
    this.borrar=false;
  }


  guardar(nombre:string, apellido:string, edad:string):boolean{
    this.personas.push(new persona(nombre,apellido, edad));

    return false;
  }

  limpiar():void{
    this.borrar=true;
    //console.log(true);
  }



  ngOnInit(): void {
  }

}
