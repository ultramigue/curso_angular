export class persona {
    nombre:string="pepe";
    apellido:string;
    edad:string;

    constructor (n:string, a:string, e:string){
        this.nombre=n;
        this.apellido=a;
        this.edad=e;   
        this.mostrar(); 
    }

    mostrar():void{
        console.log("Contenido de la persona:");
        console.log("Nombre  : ", this.nombre);
        console.log("Apellido: ", this.apellido);
        console.log("Edad    : ", this.edad);
    }

}