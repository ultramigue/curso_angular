import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaPersonalComponent } from './lista-personal/lista-personal.component';
import { VerPersonalComponent } from './ver-personal/ver-personal.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaPersonalComponent,
    VerPersonalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
